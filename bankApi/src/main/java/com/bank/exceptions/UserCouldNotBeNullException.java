package com.bank.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_GATEWAY)
public class UserCouldNotBeNullException extends RuntimeException {
    public UserCouldNotBeNullException() {
        super("User could not be null.");
    }
}
