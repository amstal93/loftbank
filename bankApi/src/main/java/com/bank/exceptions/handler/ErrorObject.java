package com.bank.exceptions.handler;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.Instant;

@Data
@AllArgsConstructor
public class ErrorObject {
    private Integer statusCode;

    private String message;

    private Instant timestamp;
}
