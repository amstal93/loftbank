package com.bank.exceptions.handler;

import com.bank.exceptions.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.Instant;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<ErrorObject> handleUserNotFoundException(UserNotFoundException e) {
        ErrorObject errorObject = new ErrorObject(404, "User is not found", Instant.now());
        errorObject.setStatusCode(HttpStatus.NOT_FOUND.value());
        errorObject.setMessage(e.getMessage());
        errorObject.setTimestamp(Instant.now());
        return new ResponseEntity<>(errorObject, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(AccountNotFoundException.class)
    public ResponseEntity<ErrorObject> handleAccountNotFoundException(AccountNotFoundException e) {
        ErrorObject errorObject = new ErrorObject(404, "Account is not found", Instant.now());
        errorObject.setStatusCode(HttpStatus.NOT_FOUND.value());
        errorObject.setMessage(e.getMessage());
        errorObject.setTimestamp(Instant.now());
        return new ResponseEntity<>(errorObject, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(TransactionNotFoundException.class)
    public ResponseEntity<ErrorObject> handleTransactionNotFoundException(TransactionNotFoundException e) {
        ErrorObject errorObject = new ErrorObject(404, "Transaction is not found", Instant.now());
        errorObject.setStatusCode(HttpStatus.NOT_FOUND.value());
        errorObject.setMessage(e.getMessage());
        errorObject.setTimestamp(Instant.now());
        return new ResponseEntity<>(errorObject, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(AmountTransferredLessThanZeroException.class)
    public ResponseEntity<ErrorObject> handleServiceException(AmountTransferredLessThanZeroException e) {
        ErrorObject errorObject = new ErrorObject(500, "An error occurred while processing the request", Instant.now());
        errorObject.setStatusCode(HttpStatus.BAD_REQUEST.value());
        errorObject.setMessage(e.getMessage());
        errorObject.setTimestamp(Instant.now());
        return new ResponseEntity<>(errorObject, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(InsufficientAmountMoneyException.class)
    public ResponseEntity<ErrorObject> handleInSufficientAmountMoneyException(InsufficientAmountMoneyException e) {
        ErrorObject errorObject = new ErrorObject(105, "Not enough money to make a transfer on your account", Instant.now());
        errorObject.setStatusCode(HttpStatus.BAD_REQUEST.value());
        errorObject.setMessage(e.getMessage());
        errorObject.setTimestamp(Instant.now());
        return new ResponseEntity<>(errorObject, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UserAlreadyExistsException.class)
    public ResponseEntity<ErrorObject> handleUserAlreadyExistsException(UserAlreadyExistsException e) {
        ErrorObject errorObject = new ErrorObject(502, "User could not be null", Instant.now());
        errorObject.setStatusCode(HttpStatus.BAD_GATEWAY.value());
        errorObject.setMessage(e.getMessage());
        errorObject.setTimestamp(Instant.now());
        return new ResponseEntity<>(errorObject, HttpStatus.BAD_GATEWAY);
    }

    @ExceptionHandler(UserCouldNotBeNullException.class)
    public ResponseEntity<ErrorObject> handleUserCouldNotBeNullException(UserCouldNotBeNullException e) {
        ErrorObject errorObject = new ErrorObject(502, "User already exists", Instant.now());
        errorObject.setStatusCode(HttpStatus.BAD_GATEWAY.value());
        errorObject.setMessage(e.getMessage());
        errorObject.setTimestamp(Instant.now());
        return new ResponseEntity<>(errorObject, HttpStatus.BAD_GATEWAY);
    }

    @ExceptionHandler(AccountAlreadyExistsException.class)
    public ResponseEntity<ErrorObject> handleAccountAlreadyExistsException(AccountAlreadyExistsException e) {
        ErrorObject errorObject = new ErrorObject(502, "Account already exists", Instant.now());
        errorObject.setStatusCode(HttpStatus.BAD_GATEWAY.value());
        errorObject.setMessage(e.getMessage());
        errorObject.setTimestamp(Instant.now());
        return new ResponseEntity<>(errorObject, HttpStatus.BAD_GATEWAY);
    }

    @ExceptionHandler(TransactionAlreadyExistsException.class)
    public ResponseEntity<ErrorObject> handleTransactionAlreadyExistsException(TransactionAlreadyExistsException e) {
        ErrorObject errorObject = new ErrorObject(502, "Transaction already exists", Instant.now());
        errorObject.setStatusCode(HttpStatus.BAD_GATEWAY.value());
        errorObject.setMessage(e.getMessage());
        errorObject.setTimestamp(Instant.now());
        return new ResponseEntity<>(errorObject, HttpStatus.BAD_GATEWAY);
    }
}
