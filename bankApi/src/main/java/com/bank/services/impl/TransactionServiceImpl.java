package com.bank.services.impl;

import com.bank.exceptions.AmountTransferredLessThanZeroException;
import com.bank.exceptions.InsufficientAmountMoneyException;
import com.bank.exceptions.TransactionNotFoundException;
import com.bank.models.Account;
import com.bank.models.AccountState;
import com.bank.models.Transaction;
import com.bank.repositories.AccountRepository;
import com.bank.repositories.TransactionRepository;
import com.bank.services.TransactionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class TransactionServiceImpl implements TransactionService {
    private final AccountServiceImpl accountService;
    private final TransactionRepository transactionRepository;
    private final AccountRepository accountRepository;


    @Autowired
    public TransactionServiceImpl(TransactionRepository transactionRepository, AccountServiceImpl accountService, AccountRepository accountRepository) {
        this.accountService = accountService;
        this.transactionRepository = transactionRepository;
        this.accountRepository = accountRepository;
    }

    @Transactional
    public void makeTransaction(Long userId, Long sourceAccountId, Long targetAccountId, Double amountTransferred) {
        Optional<Account> sourceAccount = accountRepository.findById(sourceAccountId);
        Optional<Account> targetAccount = accountRepository.findById(targetAccountId);
        log.info("Request for a money transfer has been received from user with UserID = {} ", userId);
        if (sourceAccount.get().getBalance() >= amountTransferred && amountTransferred > 0.001 && sourceAccount.get().getState().equals(AccountState.ACTIVE) && targetAccount.get().getState().equals(AccountState.ACTIVE)) {
            sourceAccount.get().setBalance(sourceAccount.get().getBalance() - amountTransferred);
            log.info("Withdraw of money to the account in the amount of " + amountTransferred + " RUB");
            log.info("The amount of money in the sender's account is " + sourceAccount.get().getBalance() + " RUB");
            targetAccount.get().setBalance(targetAccount.get().getBalance() + amountTransferred);
            log.info("The amount of money in the recipient's account is " + targetAccount.get().getBalance() + " RUB");

            Transaction transaction = new Transaction();
            transaction.setId(transaction.getId());
            transaction.setUserId(userId);
            transaction.setOperationTime(Instant.now());
            transaction.setSourceAccountId(sourceAccountId);
            transaction.setTargetAccountId(targetAccountId);
            transaction.setAmountTransferred(amountTransferred);
            transactionRepository.save(transaction);
            log.info("Transaction with ID = {} is created", transaction.getId());
            log.info("Withdraw from AccountID = {}", sourceAccount.get().getId());
            log.info("Transfer to AccountID = {}", targetAccount.get().getId());
        } else if (!sourceAccount.get().getState().equals(AccountState.ACTIVE) || (!targetAccount.get().getState().equals(AccountState.ACTIVE))) {
            throw new IllegalStateException("Account is not ACTIVE");
        } else if (amountTransferred < 0) {
            log.info("Amount of money to transfer cannot be less than zero");
            throw new AmountTransferredLessThanZeroException("Amount of money to transfer cannot be less than zero");
        } else {
            log.info("Not enough money to make a transfer on your account");
            throw new InsufficientAmountMoneyException("Not enough money to make a transfer on your account");
        }
    }

    @Override
    public List<Transaction> getTransactions(Long userId, Long accountId, Integer offset, Integer limit) {
        List<Transaction> transactionsByUser = new ArrayList<>();
        for (Transaction transaction : transactionRepository.findAll()) {
            if ((transaction.getSourceAccountId().equals(accountId) || transaction.getTargetAccountId().equals(accountId))) {
                transactionRepository.save(transaction);
                transactionsByUser.add(transaction);
                return transactionRepository.findAllById(Collections.singleton(transaction.getId()));
            }
        }
        if (offset >= 0 && limit >= 1) {
            List<Transaction> transactionList = transactionsByUser.subList(offset, offset + limit);
            log.info("Transactions are found");
            return transactionList;
        } else {
            log.info("Transactions are not found");
            return transactionsByUser;
        }
    }

    @Override
    public Transaction getTransaction(Long transactionId) {
        Transaction existingTransaction = this.transactionRepository.findById(transactionId).orElseThrow(() -> new TransactionNotFoundException(transactionId));
        log.info("Transaction with ID = {} is found ", transactionId);
        return existingTransaction;
    }

    @Override
    public Transaction updateTransaction(Transaction transaction) {
        Transaction existingTransaction = this.transactionRepository.findById(transaction.getId()).orElseThrow(() -> new TransactionNotFoundException(transaction.getId()));
        existingTransaction.setId(transaction.getId());
        existingTransaction.setUserId(transaction.getUserId());
        existingTransaction.setOperationTime(transaction.getOperationTime());
        existingTransaction.setAmountTransferred(transaction.getAmountTransferred());
        transactionRepository.save(existingTransaction);
        log.info("Transaction with ID = {} is updated", transaction.getId());
        return existingTransaction;
    }

    @Override
    public void deleteTransaction(Long transactionId) {
        Transaction existingTransaction = this.transactionRepository.findById(transactionId)
                .orElseThrow(() -> new TransactionNotFoundException(transactionId));
        this.transactionRepository.delete(existingTransaction);
        log.info("Transaction with ID = {} is deleted", transactionId);
    }
}




