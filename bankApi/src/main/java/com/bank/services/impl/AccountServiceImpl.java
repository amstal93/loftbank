package com.bank.services.impl;

import com.bank.exceptions.AccountNotFoundException;
import com.bank.exceptions.UserCouldNotBeNullException;
import com.bank.models.Account;
import com.bank.models.AccountState;
import com.bank.models.User;
import com.bank.repositories.AccountRepository;
import com.bank.repositories.UserRepository;
import com.bank.services.AccountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static com.bank.models.AccountState.ACTIVE;
import static com.bank.models.AccountState.CLOSED;

@Service
@Slf4j
public class AccountServiceImpl implements AccountService {
    private final AccountRepository accountRepository;
    private final UserServiceImpl userService;
    private final UserRepository userRepository;

    @Autowired
    public AccountServiceImpl(AccountRepository accountRepository, UserServiceImpl userService, UserRepository userRepository) {
        this.accountRepository = accountRepository;
        this.userService = userService;
        this.userRepository = userRepository;
    }

    @Transactional
    public Account createAccount(Long userId, Double initialBalance) {
        if (userId == null) {
            throw new UserCouldNotBeNullException();
        } else {
            if (initialBalance >= 0) {
                Account account = new Account();
                account.setId(accountRepository.count());
                account.setUserId(userId);
                account.setBalance(initialBalance);
                account.setState(ACTIVE);
                account.setDateCreated(LocalDateTime.now());
                accountRepository.save(account);
                log.info("Account with ID = {} is created", account.getId());
                return account;
            } else {
                throw new NumberFormatException("Initial balance could not be less than zero.");
            }
        }
    }

    @Override
    public List<Account> getAccounts(Long userId) {
        List<Account> accountsByUser = new ArrayList<>();
        for (Account account : accountRepository.findAll()) {
            if (account.getUserId().equals(userId)) {
                accountRepository.save(account);
                accountsByUser.add(account);
                log.info("Accounts with UserID = {} are found", userId);
            }
        }
        log.info("Accounts with UserID = {} are not found ", userId);
        return accountsByUser;
    }

    @Transactional(readOnly = true)
    public List<Account> getAccountsByUserId(User user) {
        return accountRepository.findAccountByUserId(user);
    }

    @Transactional(readOnly = true)
    public Account getAccount(Long accountId) {
        Account existingAccount = this.accountRepository.findById(accountId).orElseThrow(() -> new AccountNotFoundException(accountId));
        log.info("Account with ID = {} is found ", accountId);
        return existingAccount;
    }

    @Override
    public void blockAccount(Long userId, Long accountId) {
        try {
            if (userId == null) {
                throw new AccountNotFoundException(accountId);
            } else {
                Account account = getAccount(accountId);
                switch (account.getState()) {
                    case CLOSED:
                        throw new IllegalStateException("Account is already CLOSED");
                    case BLOCKED:
                        throw new IllegalStateException("Account is already BLOCKED.");
                    case ACTIVE:
                        account.setState(AccountState.BLOCKED);
                        this.accountRepository.save(account);
                        log.info("Account with ID = {} is BLOCKED", account.getId());
                }
            }
        } catch (IllegalArgumentException e) {
            System.out.println("Error " + e);
        }
    }

    @Override
    public void unblockAccount(Long userId, Long accountId) {
        try {
            if (userId == null) {
                throw new AccountNotFoundException(accountId);
            } else {
                Account account = getAccount(accountId);
                switch (account.getState()) {
                    case CLOSED:
                        throw new IllegalStateException("Account is already CLOSED");
                    case ACTIVE:
                        throw new IllegalStateException("Account is already ACTIVE");
                    case BLOCKED:
                        account.setState(ACTIVE);
                        this.accountRepository.save(account);
                        log.info("Account with ID = {} is ACTIVE", account.getId());
                }
            }
        } catch (IllegalArgumentException e) {
            System.out.println("Error " + e);
        }
    }

    @Override
    public void closeAccount(Long userId, Long accountId) {
        try {
            if (userId == null) {
                throw new AccountNotFoundException(accountId);
            } else {
                Account account = getAccount(accountId);
                switch (account.getState()) {
                    case CLOSED:
                        throw new IllegalStateException("Account is already CLOSED");
                    case BLOCKED:
                        account.setState(CLOSED);
                        this.accountRepository.save(account);
                        log.info("Account with ID = {} is CLOSED", account.getId());
                    case ACTIVE:
                        account.setState(CLOSED);
                        this.accountRepository.save(account);
                        log.info("Account with ID = {} is CLOSED", account.getId());
                }
            }
        } catch (IllegalArgumentException e) {
            System.out.println("Error " + e);
        }
    }

    @Override
    public Account updateAccount(Long accountId, Account account) {
        Account existingAccount = this.accountRepository.findById(accountId).orElseThrow(() -> new AccountNotFoundException(accountId));
        existingAccount.setUserId(account.getUserId());
        existingAccount.setBalance(account.getBalance());
        existingAccount.setState(account.getState());
        existingAccount.setDateCreated(account.getDateCreated());
        this.accountRepository.save(existingAccount);
        log.info("Account with ID = {} is updated", accountId);
        return existingAccount;
    }

    @Override
    public void deleteAccount(Long accountId) {
        Account existingAccount = this.accountRepository.findById(accountId)
                .orElseThrow(() -> new AccountNotFoundException(accountId));
        this.accountRepository.delete(existingAccount);
        log.info("Account with ID = {} is deleted", accountId);
    }
}
