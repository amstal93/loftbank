package com.bank.services;

import com.bank.models.Account;

import java.util.List;

public interface AccountService {
    Account createAccount(Long userId, Double initialBalance);

    Account getAccount(Long accountId);

    List<Account> getAccounts(Long userId);

    void blockAccount(Long userId, Long accountId);

    void unblockAccount(Long userId, Long accountId);

    void closeAccount(Long userId, Long accountId);

    Account updateAccount(Long accountId, Account account);

    void deleteAccount(Long accountId);
}

