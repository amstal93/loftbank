package com.bank.controllers;

import com.bank.dto.UserDTO;
import com.bank.models.User;
import com.bank.services.UserService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/users")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;
    private final ModelMapper modelMapper;

    @PostMapping
    public ResponseEntity<UserDTO> createUser(@RequestBody UserDTO userDTO) {
        // convert DTO to user
        User userRequest = modelMapper.map(userDTO, User.class);
        User user = userService.createUser(userRequest);
        // convert user to DTO
        UserDTO userResponse = modelMapper.map(user, UserDTO.class);
        return new ResponseEntity<>(userResponse, HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserDTO> getUser(@PathVariable("id") Long userId) {
        User user = userService.getUser(userId);
        // convert user to DTO
        UserDTO userResponse = modelMapper.map(user, UserDTO.class);
        return ResponseEntity.ok().body(userResponse);
    }

    @PutMapping("/{id}")
    public ResponseEntity<UserDTO> updateUser(@RequestBody UserDTO userDTO, @PathVariable("id") Long userId) {
        // convert DTO to user
        User userRequest = modelMapper.map(userDTO, User.class);
        User user = userService.updateUser(userRequest, userId);
        // user to DTO
        UserDTO userResponse = modelMapper.map(user, UserDTO.class);
        return ResponseEntity.ok().body(userResponse);
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable("id") Long id) {
        userService.deleteUser(id);
    }

    @PutMapping("/{id}/block")
    public void blockUser(@PathVariable("id") Long id) {
        userService.blockUser(id);
    }

    @PutMapping("/{id}/unblock")
    public void unblockUser(@PathVariable("id") Long id) {
        userService.unblockUser(id);
    }
}