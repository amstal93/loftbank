package com.bank.controllers;

import com.bank.dto.AccountDTO;
import com.bank.models.Account;
import com.bank.models.User;
import com.bank.services.AccountService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/accounts")
@RequiredArgsConstructor
public class AccountController {
    private final AccountService accountService;
    private final ModelMapper modelMapper;

    @PostMapping
    public ResponseEntity<AccountDTO> createAccount(@RequestBody AccountDTO accountDTO) {
        // convert DTO to account
        Account accountRequest = modelMapper.map(accountDTO, Account.class);
        Account account = accountService.createAccount(accountRequest.getUserId(), accountRequest.getBalance());
        // convert account to DTO
        AccountDTO accountResponse = modelMapper.map(account, AccountDTO.class);
        return new ResponseEntity<>(accountResponse, HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<AccountDTO> getAccount(@PathVariable("id") Long accountId) {
        Account account = accountService.getAccount(accountId);
        // convert account to DTO
        AccountDTO accountResponse = modelMapper.map(account, AccountDTO.class);
        return ResponseEntity.ok().body(accountResponse);
    }

    @PutMapping("/{id}")
    public ResponseEntity<AccountDTO> updateAccount(@RequestBody AccountDTO accountDTO) {
        // convert DTO to account
        Account accountRequest = modelMapper.map(accountDTO, Account.class);
        Account account = accountService.updateAccount(accountRequest.getId(), accountRequest);
        // account to DTO
        AccountDTO accountResponse = modelMapper.map(account, AccountDTO.class);
        return ResponseEntity.ok().body(accountResponse);
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable("id") Long id) {
        accountService.deleteAccount(id);
    }

    @PutMapping("/{id}/block")
    public void blockAccount(User user, Account account) {
        accountService.blockAccount(user.getId(), account.getId());
    }

    @PutMapping("/{id}/unblock")
    public void unblockAccount(User user, Account account) {
        accountService.unblockAccount(user.getId(), account.getId());
    }

    @PutMapping("/{id}/close")
    public void closeAccount(User user, Account account) {
        accountService.closeAccount(user.getId(), account.getId());
    }
}