package com.bank.models;

public enum Role {
    USER, ADMIN
}
