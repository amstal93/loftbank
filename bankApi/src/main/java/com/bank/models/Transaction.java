package com.bank.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name = "transactions")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Transaction {
    @Id
    @SequenceGenerator(name = "transactionsIdSeq", sequenceName = "transactions_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "transactionsIdSeq")
    private Long id;
    private Long userId;
    private Instant operationTime;
    private Long sourceAccountId;
    private Long targetAccountId;
    private Double amountTransferred;

    public Transaction(Long id, Long userId, Long sourceAccountId, Long targetAccountId, Double amountTransferred) {
        this.id = id;
        this.userId = userId;
        this.sourceAccountId = sourceAccountId;
        this.targetAccountId = targetAccountId;
        this.amountTransferred = amountTransferred;
    }
}