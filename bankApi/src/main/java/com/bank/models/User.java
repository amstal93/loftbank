package com.bank.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "users")
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class User {
    @Id
    @SequenceGenerator(name = "usersIdSeq", sequenceName = "users_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "usersIdSeq")
    private Long id;
    private String email;
    private String password;
    private String firstName;
    private String lastName;
    @Column(name = "date")
    private LocalDateTime dateCreated;
    @Enumerated(value = EnumType.STRING)
    private Role role = Role.USER;
    private boolean isBlocked;

    public User(Long id, String email, String password, String firstName, String lastName, Role role, boolean isBlocked) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.role = role;
        this.isBlocked = isBlocked;
    }
}
