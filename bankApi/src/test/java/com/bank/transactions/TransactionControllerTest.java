package com.bank.transactions;

import com.bank.controllers.TransactionController;
import com.bank.exceptions.TransactionNotFoundException;
import com.bank.models.*;
import com.bank.services.impl.TransactionServiceImpl;
import com.bank.utils.JSONUtils;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDateTime;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(TransactionController.class)
public class TransactionControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TransactionServiceImpl transactionService;

    User Billy = new User(1L, "billy@mail.ru", "idol", "Billy", "Idol", LocalDateTime.now(), Role.USER, false);
    Account userId1Account1 = new Account(1L, Billy.getId(), 0.0, AccountState.ACTIVE, LocalDateTime.now());
    Account userId1Account2 = new Account(2L, Billy.getId(), 10000.0, AccountState.ACTIVE, LocalDateTime.now());

    User Joseph = new User(2L, "joseph@mail.ru", "stalin", "Joseph", "Stalin", LocalDateTime.now(), Role.ADMIN, false);
    Account userId2Account1 = new Account(3L, Joseph.getId(), 25000.0, AccountState.ACTIVE, LocalDateTime.now());
    Account userId2Account2 = new Account(4L, Joseph.getId(), 75000.0, AccountState.ACTIVE, LocalDateTime.now());

    Transaction transaction = new Transaction(5L, Joseph.getId(), userId1Account2.getId(), userId1Account1.getId(), 3000.0);

    @Test
    public void controllerInitializedCorrectly() {
        Assertions.assertThat(TransactionController.class).isNotNull();
    }

    @Test
    public void testMakeTransaction() throws Exception {
        doNothing().when(transactionService).makeTransaction(anyLong(), anyLong(), anyLong(), anyDouble());
        transactionService.makeTransaction(1L, userId2Account2.getId(), userId2Account1.getId(), 75000.0);

        Mockito.verify(transactionService, times(1)).makeTransaction(1L, userId2Account2.getId(), userId2Account1.getId(), 75000.0);
    }

    @Test
    public void testGetTransactionPositive() throws Exception {
        given(transactionService.getTransaction(transaction.getId())).willReturn(transaction);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/transactions/" + transaction.getId().toString())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("id").value(5));
    }

    @Test
    public void testGetTransactionNegative() throws Exception {
        Transaction notExistingTransaction = new Transaction();
        notExistingTransaction.setId(80L);

        Mockito.doThrow(new TransactionNotFoundException(notExistingTransaction.getId())).when(transactionService).getTransaction(notExistingTransaction.getId());

        mockMvc.perform(MockMvcRequestBuilders.get("/api/transactions/" + notExistingTransaction.getId().toString())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testUpdateTransactionPositive() throws Exception {
        doNothing().when(transactionService).makeTransaction(1L, userId2Account2.getId(), userId2Account1.getId(), 75000.0);
        given(transactionService.updateTransaction(transaction)).willReturn(transaction);

        mockMvc.perform(put("/api/transactions/" + transaction.getId().toString())
                        .content(JSONUtils.toJSON(transaction))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("amountTransferred").value(transaction.getAmountTransferred()));
    }

    @Test
    public void testUpdateTransactionNegative() throws Exception {
        Transaction transaction = new Transaction();
        transaction.setId(80L);

        Mockito.doThrow(new TransactionNotFoundException(transaction.getId())).when(transactionService).updateTransaction(transaction);

        mockMvc.perform(put("/api/transactions/" + transaction.getId().toString())
                        .content(JSONUtils.toJSON(transaction))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDeleteTransactionPositive() throws Exception {
        doNothing().when(transactionService).deleteTransaction(transaction.getId());

        mockMvc.perform(delete("/api/transactions/" + transaction.getId().toString())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testDeleteTransactionNegative() throws Exception {
        Mockito.doThrow(new TransactionNotFoundException(80L)).when(transactionService).deleteTransaction(80L);

        mockMvc.perform(delete("/api/transactions/" + 80)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
}
