package com.bank.transactions;

import com.bank.exceptions.AmountTransferredLessThanZeroException;
import com.bank.exceptions.InsufficientAmountMoneyException;
import com.bank.exceptions.TransactionNotFoundException;
import com.bank.models.*;
import com.bank.repositories.AccountRepository;
import com.bank.repositories.TransactionRepository;
import com.bank.services.impl.AccountServiceImpl;
import com.bank.services.impl.TransactionServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
import static org.springframework.test.util.AssertionErrors.assertEquals;
import static org.springframework.test.util.AssertionErrors.assertNotEquals;

@RunWith(MockitoJUnitRunner.class)
public class TransactionServiceImplTest {
    @Mock
    private AccountRepository accountRepository;

    @Mock
    private TransactionRepository transactionRepository;

    @InjectMocks
    private TransactionServiceImpl transactionService;

    @InjectMocks
    private AccountServiceImpl accountService;

    User Billy = new User(1L, "billy@mail.ru", "idol", "Billy", "Idol", LocalDateTime.now(), Role.USER, false);
    Account userId1Account1 = new Account(1L, Billy.getId(), 0.0, AccountState.ACTIVE, LocalDateTime.now());
    Account userId1Account2 = new Account(2L, Billy.getId(), 10000.0, AccountState.ACTIVE, LocalDateTime.now());

    User Joseph = new User(2L, "joseph@mail.ru", "stalin", "Joseph", "Stalin", LocalDateTime.now(), Role.ADMIN, false);
    Account userId2Account1 = new Account(3L, Joseph.getId(), 25000.0, AccountState.ACTIVE, LocalDateTime.now());
    Account userId2Account2 = new Account(4L, Joseph.getId(), 75000.0, AccountState.ACTIVE, LocalDateTime.now());

    Transaction transaction = new Transaction(1L, Joseph.getId(), Instant.now(), userId1Account2.getId(), userId1Account1.getId(), 3000.0);

    @Test
    public void testMakeTransactionPositive() {
        when(accountRepository.findById(userId1Account1.getId())).thenReturn(Optional.of(userId1Account1));
        when(accountRepository.findById(userId1Account2.getId())).thenReturn(Optional.of(userId1Account2));

        Account account1 = accountService.getAccount(userId1Account1.getId());
        Account account2 = accountService.getAccount(userId1Account2.getId());

        assertEquals("", account1.getUserId(), userId1Account1.getUserId());
        assertEquals("", account1.getUserId(), userId1Account2.getUserId());

        assertEquals("", account1.getBalance(), userId1Account1.getBalance());
        assertEquals("", account2.getBalance(), userId1Account2.getBalance());

        transactionService.makeTransaction(Billy.getId(), account2.getId(), account1.getId(), 2000.0);
        transactionService.makeTransaction(Billy.getId(), account2.getId(), account1.getId(), 2000.0);
        transactionService.makeTransaction(Billy.getId(), account2.getId(), account1.getId(), 2000.0);
        transactionService.makeTransaction(Billy.getId(), account2.getId(), account1.getId(), 2000.0);

        assertEquals("", account1.getBalance(), 8000.0);
        assertEquals("", account2.getBalance(), 2000.0);
    }

    @Test
    public void testMakeTransactionBetweenDifferentUsersPositive() {
        when(accountRepository.findById(userId1Account1.getId())).thenReturn(Optional.of(userId1Account1));
        when(accountRepository.findById(userId2Account1.getId())).thenReturn(Optional.of(userId2Account1));

        Account account1 = accountService.getAccount(userId1Account1.getId());
        Account account2 = accountService.getAccount(userId2Account1.getId());

        System.out.println(account1.getBalance());
        System.out.println(account2.getBalance());
        assertEquals("", account1.getBalance(), 0.0);
        assertEquals("", account2.getBalance(), 25000.0);

        transactionService.makeTransaction(Billy.getId(), account2.getId(), account1.getId(), 5000.0);

        assertEquals("", account1.getBalance(), 5000.0);
        assertEquals("", account2.getBalance(), 20000.0);
    }

    @Test(expected = InsufficientAmountMoneyException.class)
    public void testMakeTransactionInsufficientAmountMoneyNegative() {
        when(accountRepository.findById(userId2Account1.getId())).thenReturn(Optional.of(userId2Account1));
        when(accountRepository.findById(userId2Account2.getId())).thenReturn(Optional.of(userId2Account2));

        Account account1 = accountService.getAccount(userId2Account1.getId());
        Account account2 = accountService.getAccount(userId2Account2.getId());

        transactionService.makeTransaction(Billy.getId(), account1.getId(), account2.getId(), 50000.0);
    }

    @Test(expected = AmountTransferredLessThanZeroException.class)
    public void testMakeTransactionAmountTransferredLessThanZeroNegative() {
        when(accountRepository.findById(userId2Account1.getId())).thenReturn(Optional.of(userId2Account1));
        when(accountRepository.findById(userId2Account2.getId())).thenReturn(Optional.of(userId2Account2));

        Account account1 = accountService.getAccount(userId2Account1.getId());
        Account account2 = accountService.getAccount(userId2Account2.getId());

        transactionService.makeTransaction(Billy.getId(), account1.getId(), account2.getId(), -10000.0);
    }

    @Test
    public void testUpdateTransactionPositive() {
        given(accountRepository.findById(userId1Account1.getId())).willReturn(Optional.of(userId1Account1));
        accountService.updateAccount(userId1Account1.getId(), userId1Account2);
        assertEquals("", userId1Account1.getBalance(), userId1Account2.getBalance());
        assertNotEquals("", userId1Account1.getBalance(), userId2Account1.getBalance());

        verify(accountRepository, times(1)).save(userId1Account1);
        verify(accountRepository).findById(userId1Account1.getId());
    }

    @Test(expected = TransactionNotFoundException.class)
    public void testUpdateTransactionNegative() {
        given(transactionRepository.findById(anyLong())).willReturn(Optional.ofNullable(null));
        transactionService.updateTransaction(transaction);
    }

    @Test
    public void testDeleteTransactionPositive() {
        when(transactionRepository.findById(transaction.getId())).thenReturn(Optional.of(transaction));
        transactionService.deleteTransaction(transaction.getId());

        verify(transactionRepository).delete(transaction);
        verify(transactionRepository, times(1)).delete(transaction);
    }

    @Test(expected = TransactionNotFoundException.class)
    public void testDeleteTransactionNegative() {
        given(transactionRepository.findById(anyLong())).willReturn(Optional.empty());
        transactionService.deleteTransaction(transaction.getId());
    }
}

