package com.bank.accounts;

import com.bank.controllers.AccountController;
import com.bank.exceptions.AccountNotFoundException;
import com.bank.models.Account;
import com.bank.models.AccountState;
import com.bank.models.Role;
import com.bank.models.User;
import com.bank.services.impl.AccountServiceImpl;
import com.bank.utils.JSONUtils;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(AccountController.class)
public class AccountControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AccountServiceImpl accountService;

    User Ozzi = new User(1L, "ozzi@mail.ru", "osborne", "Ozzi", "Osbourne", Role.USER, false);
    Account userId1Account1 = new Account(1L, Ozzi.getId(), 7_000.0, AccountState.ACTIVE);
    Account userId1Account2 = new Account(2L, Ozzi.getId(), 90_000.0, AccountState.ACTIVE);

    @Test
    public void controllerInitializedCorrectly() {
        Assertions.assertThat(AccountController.class).isNotNull();
    }

    @Test
    public void testCreateAccount() throws Exception {
        given(accountService.createAccount(Ozzi.getId(), userId1Account1.getBalance())).willReturn(userId1Account1);

        mockMvc.perform(MockMvcRequestBuilders.post("/api/accounts/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(JSONUtils.toJSON(userId1Account1))
                )
                .andExpect(status().isCreated())
                .andExpect(jsonPath("balance").value(7_000.0))
                .andExpect(jsonPath("state").value(AccountState.ACTIVE.name()));
    }

    @Test
    public void testGetAccountPositive() throws Exception {
        given(accountService.getAccount(userId1Account1.getId())).willReturn(userId1Account1);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/accounts/" + userId1Account1.getId().toString())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("id").value(1))
                .andExpect(jsonPath("userId").value(1))
                .andExpect(jsonPath("balance").value(7000.0))
                .andExpect(jsonPath("state").value(AccountState.ACTIVE.name()));
    }

    @Test
    public void testGetAccountNegative() throws Exception {
        Mockito.doThrow(new AccountNotFoundException(userId1Account1.getId())).when(accountService).getAccount(userId1Account1.getId());

        mockMvc.perform(MockMvcRequestBuilders.get("/api/accounts/" + userId1Account1.getId().toString())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testUpdateAccountPositive() throws Exception {
        given(accountService.updateAccount(userId1Account2.getId(), userId1Account2)).willReturn(userId1Account2);

        mockMvc.perform(put("/api/accounts/" + userId1Account2.getId().toString())
                        .content(JSONUtils.toJSON(userId1Account2))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("balance").value(userId1Account2.getBalance()));
    }

    @Test
    public void testUpdateAccountNegative() throws Exception {
        Account account = new Account();
        account.setId(80L);

        Mockito.doThrow(new AccountNotFoundException(account.getId())).when(accountService).updateAccount(account.getId(), account);

        mockMvc.perform(put("/api/accounts/" + account.getId().toString())
                        .content(JSONUtils.toJSON(account))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDeleteAccountPositive() throws Exception {
        doNothing().when(accountService).deleteAccount(userId1Account1.getId());

        mockMvc.perform(delete("/api/accounts/" + userId1Account1.getId().toString())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testDeleteAccountNegative() throws Exception {
        Account account = new Account();
        account.setId(80L);

        Mockito.doThrow(new AccountNotFoundException(account.getId())).when(accountService).deleteAccount(account.getId());

        mockMvc.perform(delete("/api/accounts/" + account.getId().toString())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
}
