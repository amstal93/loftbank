package com.bank.accounts;

import com.bank.exceptions.AccountNotFoundException;
import com.bank.models.Account;
import com.bank.models.AccountState;
import com.bank.models.Role;
import com.bank.models.User;
import com.bank.repositories.AccountRepository;
import com.bank.services.impl.AccountServiceImpl;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
import static org.springframework.test.util.AssertionErrors.assertEquals;
import static org.springframework.test.util.AssertionErrors.assertNotEquals;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceImplTest {

    @Mock
    private AccountRepository accountRepository;

    @InjectMocks
    private AccountServiceImpl accountService;

    User Ozzi = new User(1L, "ozzi@mail.ru", "osborne", "Ozzi", "Osbourne", Role.USER, false);
    Account userId1Account1 = new Account(1L, Ozzi.getId(), 0.0, AccountState.ACTIVE);
    Account userId1Account2 = new Account(2L, Ozzi.getId(), 10000.0, AccountState.ACTIVE);

    User Marilyn = new User(2L, "marilyn@mail.ru", "manson", "Marilyn", "Manson", Role.USER, true);
    Account userId2Account1 = new Account(3L, Marilyn.getId(), 0.0, AccountState.ACTIVE);
    Account userId2Account2 = new Account(4L, Marilyn.getId(), 50000.0, AccountState.ACTIVE);

    User Matthew = new User(3L, "matthew@mail.ru", "mcConaughey", "Matthew", "McConaughey", Role.ADMIN, false);
    Account userId3Account1 = new Account(5L, Matthew.getId(), 0.0, AccountState.ACTIVE);
    Account userId3Account2 = new Account(6L, Matthew.getId(), 20000.0, AccountState.ACTIVE);

    @Test
    public void accountServiceImplInitializedCorrectly() {
        Assertions.assertThat(AccountServiceImpl.class).isNotNull();
    }

    @Test
    public void testCreateAccountPositive() {
        Account accountCreated1 = accountService.createAccount(Ozzi.getId(), 0.0);
        Account accountCreated2 = accountService.createAccount(Ozzi.getId(), 10000.0);

        assertEquals("", accountCreated1.getUserId(), userId1Account1.getUserId());
        assertEquals("", accountCreated2.getUserId(), userId1Account2.getUserId());

        assertEquals("", accountCreated1.getBalance(), userId1Account1.getBalance());
        assertEquals("", accountCreated2.getBalance(), userId1Account2.getBalance());

        verify(accountRepository, times(1)).save(accountCreated1);
        verify(accountRepository, times(1)).save(accountCreated2);
    }

    @Test(expected = NumberFormatException.class)
    public void testCreateAccountNegative_numberFormatException() {
        Account accountIsNotCreated = accountService.createAccount(Ozzi.getId(), -100.0);
        verify(accountRepository, times(1)).save(accountIsNotCreated);
    }

    @Test(expected = AccountNotFoundException.class)
    public void testCreateAccountNegative_accountNotFoundException() {
        given(accountRepository.findById(anyLong())).willReturn(Optional.ofNullable(null));
        Account accountCreated = accountService.getAccount(80L);
        verify(accountRepository, times(1)).save(accountCreated);
    }

    @Test
    public void testBlockAndUnBlockAccountPositive() {
        // block
        when(accountRepository.findById(userId1Account1.getId())).thenReturn(Optional.of(userId1Account1));
        when(accountRepository.findById(userId1Account2.getId())).thenReturn(Optional.of(userId1Account2));

        Account account1 = accountService.getAccount(userId1Account1.getId());
        Account account2 = accountService.getAccount(userId1Account2.getId());

        accountService.blockAccount(Ozzi.getId(), account1.getId());
        accountService.blockAccount(Ozzi.getId(), account2.getId());

        assertEquals("", account1.getState(), AccountState.BLOCKED);
        assertEquals("", account2.getState(), AccountState.BLOCKED);

        verify(accountRepository).save(account1);
        verify(accountRepository, times(1)).save(account2);
        // unblock
        accountService.unblockAccount(Ozzi.getId(), account1.getId());
        accountService.unblockAccount(Ozzi.getId(), account2.getId());

        assertEquals("", account1.getState(), AccountState.ACTIVE);
        assertEquals("", account2.getState(), AccountState.ACTIVE);
    }

    @Test
    public void testCloseAccountPositive() {
        when(accountRepository.findById(userId2Account1.getId())).thenReturn(Optional.of(userId2Account1));
        when(accountRepository.findById(userId2Account2.getId())).thenReturn(Optional.of(userId2Account2));

        Account account1 = accountService.getAccount(userId2Account1.getId());
        Account account2 = accountService.getAccount(userId2Account2.getId());

        accountService.closeAccount(Matthew.getId(), account1.getId());
        accountService.closeAccount(Matthew.getId(), account2.getId());

        assertEquals("", account1.getState(), AccountState.CLOSED);
        assertEquals("", account2.getState(), AccountState.CLOSED);

        verify(accountRepository).save(account1);
        verify(accountRepository, times(1)).save(account2);
    }

    @Test(expected = IllegalStateException.class)
    public void testBlockAccountAfterCloseNegative() {
        when(accountRepository.findById(userId2Account1.getId())).thenReturn(Optional.of(userId2Account1));
        when(accountRepository.findById(userId2Account2.getId())).thenReturn(Optional.of(userId2Account2));

        Account account1 = accountService.getAccount(userId2Account1.getId());
        Account account2 = accountService.getAccount(userId2Account2.getId());

        accountService.closeAccount(Matthew.getId(), account1.getId());
        accountService.closeAccount(Matthew.getId(), account2.getId());

        assertEquals("", account1.getState(), AccountState.CLOSED);
        assertEquals("", account2.getState(), AccountState.CLOSED);

        accountService.blockAccount(Matthew.getId(), account1.getId());
        accountService.blockAccount(Matthew.getId(), account2.getId());
    }

    @Test(expected = IllegalStateException.class)
    public void testUnBlockAccountAfterCloseNegative() {
        when(accountRepository.findById(userId1Account1.getId())).thenReturn(Optional.of(userId1Account1));
        when(accountRepository.findById(userId1Account2.getId())).thenReturn(Optional.of(userId1Account2));

        Account account1 = accountService.getAccount(userId1Account1.getId());
        Account account2 = accountService.getAccount(userId1Account2.getId());

        accountService.closeAccount(Ozzi.getId(), account1.getId());
        accountService.closeAccount(Ozzi.getId(), account2.getId());

        assertEquals("Account is CLOSED", account1.getState(), AccountState.CLOSED);
        assertEquals("Account is CLOSED", account2.getState(), AccountState.CLOSED);

        accountService.unblockAccount(Matthew.getId(), account1.getId());
        accountService.unblockAccount(Matthew.getId(), account2.getId());
    }

    @Test
    public void testUpdateAccountPositive() {
        given(accountRepository.findById(userId1Account1.getId())).willReturn(Optional.of(userId1Account1));
        accountService.updateAccount(userId1Account1.getId(), userId1Account2);
        assertEquals("", userId1Account1.getBalance(), userId1Account2.getBalance());
        assertNotEquals("", userId1Account1.getBalance(), userId2Account1.getBalance());

        verify(accountRepository, times(1)).save(userId1Account1);
        verify(accountRepository).findById(userId1Account1.getId());
    }

    @Test(expected = AccountNotFoundException.class)
    public void testUpdateAccountNegative() {
        given(accountRepository.findById(anyLong())).willReturn(Optional.ofNullable(null));
        accountService.updateAccount(userId3Account1.getId(), userId1Account2);
    }

    @Test
    public void testDeleteAccountPositive() {
        when(accountRepository.findById(userId3Account2.getId())).thenReturn(Optional.of(userId3Account2));
        accountService.deleteAccount(userId3Account2.getId());

        verify(accountRepository).delete(userId3Account2);
        verify(accountRepository, times(1)).delete(userId3Account2);
    }

    @Test(expected = AccountNotFoundException.class)
    public void testDeleteAccountNegative() {
        given(accountRepository.findById(anyLong())).willReturn(Optional.empty());
        accountService.deleteAccount(userId1Account1.getId());
    }
}